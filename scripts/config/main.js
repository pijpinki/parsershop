/**
 * Created by pinki on 02.06.2017.
 */
let options = {
    photos_path     : '/var/www/snikerhad.bel/media/catalog/product/all/', // Место где сохраняються фотограмии относительно app.js, пример на движке [/var/www/site/images]
    photos_site_path: "/all/", // Место по которому будет записано в базе [public/images]
    timeout         : 60 * 60 * 1000,
    currency        : 0.57
};

module.exports.options = options;