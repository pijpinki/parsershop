/**
 * Created by pinki on 02.06.2017.
 */
let mysql      = require('mysql2');

let main       = {
    user : "root",
    pass : "123qqq",
    host : "localhost",
    base : "parser"
};

let magento = {
    user : 'root',
    pass : '123qqq',
    host : "localhost",
    base : "magento_new"
};

let connection = mysql.createConnection({
    host     : main.host,
    user     : main.user,
    password : main.pass,
    database : main.base
});

let magentoConnection = mysql.createPool({
    host     : magento.host,
    user     : magento.user,
    password : magento.pass,
    database : magento.base
});

module.exports.main       = main;
module.exports.connection = connection;
module.exports.magentoConnection = magentoConnection;
