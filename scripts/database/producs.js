/**
 * Created by pinki on 02.06.2017.
 */
let connection = require('../config/database').connection;

/**
 *
 * @param params
 * @param params.number
 * @param callback
 */
function get(params, callback) {
    let sql = "SELECT * FROM products WHERE number = ?";
    connection.query(sql, [params.number], (err, products) => {
        callback(err, products);
    });
}


/**
 * @param params
 * @param params.name
 * @param params.number
 * @param params.product_image_url
 * @param params.unit_price
 * @param params.unit_sale_price
 * @param params.product_code
 * @param callback
 */
function add(params, callback) {
    let insert = {
        name             : params.name,
        unit_sale_price  : params.unit_sale_price,
        unit_price       : params.unit_price,
        number           : params.number,
        product_code     : params.product_code,
        product_image_url: params.product_image_url
    };
    let sql    = "INSERT INTO products SET ?";
    connection.query(sql, insert, (err, data) => {
        callback(err, data);
    });
}

function update(params, callback) {
    let sql = `UPDATE products 
    SET name = '${params.name}', 
    product_image_url = '${params.product_image_url}', 
    unit_price = ${params.unit_price},
    unit_sale_price = ${params.unit_sale_price}, 
    product_code = '${params.product_code}', 
    new = 1 
    WHERE number = '${params.product_number}'`;

    connection.query(sql, (err, data) => {
        callback(err, data);
    });
}

function reset() {
    let sql = "UPDATE products SET new = 0";
    connection.query(sql, err => {
        if (err) console.error(err);
    });
}

function getNew(callback) {
    let sql = "SELECT * FROM products WHERE new <> 1";
    connection.query(sql, (err, data) => {
        callback(err, data);
    });
}

function getAll(callback) {
    let sql = "SELECT products.* FROM products";
    connection.query(sql, (err, data) => {
        callback(err, data);
    });
}

function getUpdated(callback) {
    let sql = "SELECT products.* FROM products WHERE new = 1";
    connection.query(sql, (err, data) => {
        callback(err, data);
    });
}

module.exports.get    = get;
module.exports.add    = add;
module.exports.reset  = reset;
module.exports.update = update;

module.exports.getNew     = getNew;
module.exports.getAll     = getAll;
module.exports.getUpdated = getUpdated;
