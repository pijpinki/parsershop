/**
 * Created by pinki on 14.06.2017.
 */
let connection = require('../../config/database').magentoConnection;
const logger = require('log4js').getLogger('Magento database');

/**
 *
 * @param params
 * @param params.product_id
 * @param params.image_url
 * @param callback
 */
function addImages(params, callback) {
    let sql    = "INSERT INTO `catalog_product_entity_media_gallery` (`attribute_id`, `entity_id`, `value`) VALUES ('88', ?, ?)";
    let insert = [params.product_id, params.image_url];
    if(params.product_id && params.image_url) {
        connection.query(sql, insert, (err, data) => {
            callback(err, data)
        });
    }else {
        callback(null, true);
    }
}

function removeImages(params, callback) {
    let sql = "DELETE FROM `catalog_product_entity_media_gallery` WHERE `entity_id` = ?";
    connection.query(sql, [params.product_id], (err, data) => {
        if(err){
            if(err.Error.indexOf("Deadlock found when trying to get lock; try restarting transaction") != -1){
                logger.warn('Схватили deadlock повторная попытка')
                return removeImages(params,callback);
            } else
                return callback(err)
        }
       callback(err, data);
    });
}

module.exports.addImages    = addImages;
module.exports.removeImages = removeImages;