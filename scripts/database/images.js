/**
 * Created by pinki on 05.06.2017.
 */
let connection = require("../config/database").connection;

/**
 *
 * @param params
 * @param params.product_id
 * @param params.image_url
 * @param params.stock_image_url
 * @param callback
 */
function add(params, callback) {
    let sql = "INSERT INTO product_images SET ?";
    connection.query(sql, params, (err, data) => {
        callback(err, data);
    });
}

function get(params, callback) {
    let sql = "SELECT * FROM product_images WHERE product_id = ?";
    connection.query(sql, [params.product_id], (err, sizes) => {
        callback(err, sizes);
    });
}

function getById(params, callback) {
    let sql = "SELECT * FROM product_images WHERE id = ?";
    connection.query(sql, [params.id], (err, images) => {
        callback(err, images);
    });
}

function remove(params, callback) {
    let sql = "DELETE FROM product_images WHERE product_id = ?";
    connection.query(sql, [params.product_id], (err, data) => {
        callback(err, data);
    });
}

module.exports.add     = add;
module.exports.get     = get;
module.exports.remove  = remove;
module.exports.getById = getById;

