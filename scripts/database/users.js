/**
 * Created by pinki on 02.06.2017.
 */
let connection = require('../config/database').connection;

function getUserByLoginPass(params, callback) {
    let sql = "SELECT * FROM users WHERE login = ?";
    connection.query(sql, [params.login], function (err, data) {
        callback(err, data);
    });
}

function getUserByToken(params, callback) {
    let sql = "SELECT * FROM users WHERE token = ?";
    connection.query(sql, [params.token], function (err, users) {
        callback(err, users);
    });
}

module.exports.getUserByLoginPass = getUserByLoginPass;
module.exports.getUserByToken     = getUserByToken;