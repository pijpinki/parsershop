/**
 * Created by pinki on 05.06.2017.
 */
let connection = require('../config/database').connection;


/**
 *
 * @param params
 * @param params.product_id
 * @param params.size
 * @param params.size_us
 * @param params.size_uk
 * @param params.size_eu
 * @param params.size_cm
 * @param callback
 */
function add(params, callback) {
    let sql = "INSERT INTO product_sizes SET ?";
    connection.query(sql, params, (err, data) => {
        callback(err, data);
    });
}

function get(params, callback) {
    let sql = "SELECT * FROM product_sizes WHERE product_id = ?";
    connection.query(sql, [params.product_id], (err, sizes) => {
        callback(err, sizes);
    });
}

/**
 * Удаление
 * @param params
 * @param params.product_id
 * @param callback
 */
function remove(params, callback) {
    let sql = "DELETE FROM product_sizes WHERE product_id = ?";
    connection.query(sql, [params.product_id], (err, data) => {
        callback(err, data);
    });
}

module.exports.add    = add;
module.exports.get    = get;
module.exports.remove = remove;
