let connection = require("../config/database").connection;
let async       = require('async');

class colors {

    static add(params, callback){
        let sql = "INSERT INTO `prduct_colors` SET ?";
        connection.query(sql, params,
            (err, data) => {
                if(err) {
                    return callback(err);
                }

                callback(null, data);
            });
    }

    static update(params, _callback){
        function add(callback) {
            colors.add(params, callback);
        }

        function remvoe(callback) {
            colors.remove(params, callback);
        }

        let functions = [remvoe, add];


        async.series(functions, _callback);
    }

    static remove(param, callback){
        let sql = "DELETE FROM `prduct_colors` WHERE product_id = ?";
        connection.query(sql, [param.product_id], (err, data) => {
            callback(err, data)
        })
    }

}

module.exports = colors;