/**
 * Created by pinki on 02.06.2017.
 */
let connection = require('../config/database').connection;

function getStatus(callback) {
    let sql = "SELECT * FROM `parser` WHERE id = 1";

    connection.query(sql, function (err, data) {
        callback(err, data);
    });
}

function updateStatus(params, callback) {
    let sql1 = "UPDATE `parser` SET `status` = 1,`end_time` = CURRENT_TIMESTAMP WHERE id = 1";
    let sql2 = "UPDATE `parser` SET `status` = 0, `last_added` = ?, `last_updated` = ?, `chacked` = ? WHERE id = 1";
    let sql  = "";

    if(params.status === 1)
        sql  = sql1;
    else
        sql  = sql2;


    connection.query(sql,[params.last_added, params.last_updated, params.chacked], function (err, data) {
        callback(err, data);
    });
}

function resetStatus() {
    connection.query("UPDATE parser SET `status` = 0 WHERE id = 1", err => {err && console.error(err)});
}

module.exports.getStatus    = getStatus;
module.exports.updateStatus = updateStatus;
module.exports.resetStatus  = resetStatus;