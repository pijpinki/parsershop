let needle   = require('needle');
let async    = require('async');
let parser   = require('./parserV3');
let fs       = require('fs');
let logger   = require('./loger');
const config = require('../config/magento');

/**
 * Parsing producs form site
 * @param url
 * @param callback
 */
class finder {
    constructor(url, callback, pages, page, categories) {
        this.startPage  = 0;
        this.endPage    = 0;
        this.url        = url;
        this.links      = [];
        this.products   = [];
        this.callback   = callback;
        this.pages      = pages || false;
        this.page       = page || 0;
        this.categories = categories;
    }

    /**
     * Return start and end page IND
     * @param callback
     */
    getPagesCount(callback) {
        logger.info('finderV3.getPagesCount get pages');
        logger.info('Получаем количество страниц');
        if (this.pages) {
            logger.warn('Старница нового обрасца, новый код');
            return callback(null, "big page");
        }

        needle.get(this.url, {timeout: config.timeout}, err => {
            if (err) {
                logger.error('finderV3.getPagesCount', err);
                return callback(err);
            }

            callback(null, "pages count");
        });
    }

    /**
     * Return objects Of LLink For Current Page
     * links : []
     * @param _callback
     */
    getLinksFromPages(_callback) {
        logger.info('Получаем ссылки со страницы');

        let _this = this;

        this.endPage > 0 ? manyPages(_this.startPage) : singlePage();

        function manyPages(page) {
            logger.debug('Сайт имеент много страничную систему');

            if (page < _this.endPage)
                getLinks(page, () => manyPages(page + 1));
            else
                _callback(null, "links");
        }

        function singlePage() {
            logger.debug('Одностраничный сайт с подргурзкой');
            getLinks(_this.page, () => _callback(null, "links"));
        }

        function getLinks(page, callback) {
            let url = _this.url + "," + page;

            logger.debug('Пулучаем ссылки со страницы', url);

            needle.get(url, {timeout: config.timeout}, (err, data) => {
                if (err) {
                    logger.error(err);
                    return callback();
                }

                let Parser = new parser(data.body, 'links');
                let l      = Parser.getLinks();
                logger.debug(`Получили ${l.length} продуктов`);

                l.forEach(link => _this.links.push(link));

                callback();
            });
        }

    }

    /**
     * Adding to database a
     * @param _callback
     */
    getProdctsDetals(_callback) {
        logger.info('Получаем инфу о продукте');
        let _this = this;

        if (_this.links.length > 0)
            getOne();
        else
            _callback(null, "empty");

        function getOne() {
            logger.debug('Генериуем функции');
            // async.parallelLimit(functions,40,done.bind(_this));
            async.eachLimit(_this.links, 40, get, err => {
                if (err)
                    logger.error(err);

                _callback(null, true);
            });
        }

        function get(url, callback) {
            needle.get(url, {timeout: config.timeout}, (err, res) => {
                if (err)
                    logger.error(err);

                logger.debug('Парсим продукт', url);
                const Parser = new parser(res.body, 'product');

                Parser.getProduct((err, product) => {
                    if (err) {
                        logger.error(err);
                        return callback(null, true);
                    }

                    product.category_ids = _this.categories;
                    _this.products.push(product);

                    logger.debug('Успешнл', url);
                    callback(null, true);
                });
            });

        }
    }

    /**
     * Start parcing all
     */
    start() {
        let functions = [
            this.getPagesCount.bind(this),
            this.getLinksFromPages.bind(this),
            this.getProdctsDetals.bind(this)
        ];

        function done(err) {
            if (err)
                logger.fatal(err);

            logger.info('Парсинг сайта закончен, число продуктов', this.products.length);
            fs.writeFileSync('products', JSON.stringify(this.products));
            logger.info('check file');
            this.callback(err, this.products);
        }

        async.series(functions, done.bind(this));
    }

}

module.exports = finder;