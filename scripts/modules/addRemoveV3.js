let fs    = require('fs');
let async = require('async');

let productDB = require('../database/producs');
let imagesDB  = require('../database/images');
let colorsDB  = require('../database/colors');
const sizesDB = require('../database/sizes');

const logger = require('log4js').getLogger('addRemoveV3');
logger.level = 'all';

class addRemoveV3 {
    constructor(products, callback) {
        this.products = products || fs.readFileSync("./data/finish-products.json");
        this.callback = callback;

        this.product = {};

        this.stats         = {};
        this.stats.updated = 0;
        this.stats.added   = 0;
    }

    detect(product, callback) {
        if (!product || typeof product !== 'object') {
            logger.warn('У продукта нехватает полей, пропускаем его');
            return callback(null, null);
        }

        this.product              = product;
        this.product.product_code = product.product_code || product.sku;
        logger.info('Обределяем, обновить или добавить', product.product_code, product.id);

        if (this.product.id && this.product.product_code && this.product.images && this.product.images.length) {
            logger.debug('У продукта есть небходимые поля и изображения');
            if (this.product.type === "new" || !this.product.dbId)
                this.add(callback);
            else
                this.update(callback);
        } else {
            logger.warn('У продукта нехватает полей, пропускаем его');
            callback(null, null);
        }
    }

    add(callback) {
        logger.info('Добавить продукт в базу');
        let functions = [
            this.addProduct.bind(this),
            this.addImages.bind(this),
            this.addColors.bind(this),
            this.addSizes.bind(this)
        ];

        function done(err) {
            logger.error(err, this.product.sku);

            logger.debug('Готово');
            callback(null, true);
        }

        async.series(functions, done.bind(this));

    }

    update(callback) {
        logger.info('Обновить продукт в базе');
        let functions = [
            this.updateProduct.bind(this),
            this.updateImages.bind(this),
            this.updateColors.bind(this),
            this.updateSizes.bind(this)
        ];

        function done(err) {
            err ? logger.error(err) : logger.debug('Обновлен');
            callback(null, true);
        }

        async.series(functions, done);
    }

    addImages(_callback) {
        const product_id       = this.product.dbId;
        const imagesToDatabase = [];

        for (const image of this.product.images) {
            imagesToDatabase.push({
                product_id     : product_id,
                image_url      : image.url,
                stock_image_url: image.url
            });
        }

        const addImage = (image, clb) => imagesDB.add(image, clb);

        async.eachSeries(imagesToDatabase, addImage, _callback);
    }

    addColors(_callback) {
        const colorsToDatabase = [];

        for (const color of this.product.colors) {
            colorsToDatabase.push({
                product_id : this.product.dbId,
                color_id   : color.id,
                color_name : color.title,
                color_link : color.link,
                color_image: color.image
            });
        }

        const addColor = (color, clb) => colorsDB.add(color, clb);

        async.each(colorsToDatabase, addColor, _callback);
    }

    addProduct(callback) {
        let _this = this;
        productDB.add({
            name             : this.product.name,
            unit_sale_price  : this.product.sale,
            unit_price       : this.product.price,
            number           : this.product.sku,
            product_code     : this.product.id,
            product_image_url: this.product.images.length > 0 ? this.product.images[0].url : ""
        }, (err, data) => {
            if (err)
                return callback(err);

            _this.product.dbId = data.insertId;
            _this.stats.added++;

            callback(null, data.insertId);
        });
    }

    addSizes(callback) {
        const addOne = (size, clb) => {
            sizesDB.add(Object.assign({}, {product_id: this.product.dbId}, size), clb);
        };

        async.each(this.product.sizes, addOne.bind(this), callback);
    }

    updateSizes(callback) {
        const deleteAll = clb => {
            sizesDB.remove({product_id: this.product.dbId}, clb);
        };

        const addAll = clb => {
            this.addSizes(clb);
        };

        async.series([deleteAll.bind(this), addAll.bind(this)], callback);
    }

    updateImages(callback) {
        imagesDB.remove({product_id: this.product.dbId}, (err) => {
            if (err)
                return callback(err);

            this.addImages(callback);
        });
    }

    updateColors(callback) {
        colorsDB.remove({product_id: this.product.dbId}, err => {
            if (err)
                return callback(err);

            this.addColors(callback);
        });
    }

    updateProduct(callback) {
        productDB.update({
            name             : this.product.title,
            product_image_url: this.product.images ? this.product.images[0].url : "",
            unit_price       : this.product.price,
            unit_sale_price  : this.product.sale,
            product_code     : this.product.sku,
            number           : this.product.id
        }, (err, data) => {
            if (err)
                return callback(err);

            this.stats.updated++;

            callback(null, data);
        });
    }

    start() {
        let _this = this;

        if (this.products.length > 0) {
            one(0);
        } else {
            this.callback(null, []);
        }

        function one(index) {
            if (index < _this.products.length) {
                _this.detect(_this.products[index], () => one(index + 1));
            } else {
                console.log(_this.stats);
                _this.callback(null, _this.products);
            }
        }
    }
}

module.exports = addRemoveV3;