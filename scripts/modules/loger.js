let config = require('../config/loger');
const log4js = require('log4js');

if(config.type == 'file') {
    let config = {everything: {type: 'file', filename: 'all-the-logs.log'}};
    log4js.configure({
        appenders: config,
        categories: {default: {appenders: ['everything'], level: 'all'}}
    });
}
let logger = log4js.getLogger();
logger.level = 'all';
logger.info('Logger enabled');
module.exports = logger;


