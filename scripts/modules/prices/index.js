const {currency} = require('../../config/main');

class Prices {
    /**
     * Get Bel from zlote
     * @param {(number | null)} zlote - Цена в злотых
     * @return {(number | null)}
     */
    static getPrice(zlote) {
        if (!zlote)
            return zlote;

        zlote = parseFloat(zlote);

        if (zlote < 250)
            return zlote * 0.8 * 1.05 / 1.23 / currency + 48;
        else if (zlote > 250 && zlote < 350)
            return zlote * 0.8 * 1.05 / 1.23 / currency + 50;
        else
            return zlote * 0.8 * 1.05 / 1.23 / currency + 58;
    }
}

module.exports = Prices;