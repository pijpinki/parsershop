/**
 * Created by pinki on 13.06.2017.
 */
let config       = require("../config/magento");
let Images       = require("../database/magento/images");
let magentoV2    = require('./magentoV2');
let MagentoAPI   = require('magento');
let lang         = require('../config/lang');
let logic        = require('./main');
let fs           = require('fs');
let async        = require('async');
let logger       = require('log4js').getLogger('magento');
const base64     = require('base64-img');
const configMain = require('../config/main');

magentoV2 = new magentoV2();
var file  = "log.txt";

function auth() {
    console.log(config.siteBase);
    let magento = new MagentoAPI({
        host : config.siteBase,
        port : 80,
        path : config.apiPath,
        login: config.user,
        pass : config.password
    });
    magento.login(function (err, sessId) {
        console.log(err);
        createTestProduct(magento, {images: [], sizes: []});
    });
}

function fixProducts(product) {
    const sale = product.price_save;
    const orig = product.price_stock;

    if (sale && sale > orig) {
        product.price_stock = sale;
        product.price_save  = orig;
    }

    return product;
}

function startAddProducts(products = [], done, info) {
    logger.debug('Magento start add products');
    if (!products || !products.length)
        return done();

    try {
        products = products.map(fixProducts);
    } catch (e) {
        logger.error(e)
        logger.debug(products);
        return done();
    }

    let magento = new MagentoAPI({
        host : config.siteBase,
        port : 80,
        path : config.apiPath,
        login: config.user,
        pass : config.password
    });
    magento.login(function (err, sessId) {
        if (err)
            throw err;

        logger.debug('Logined to magento class', sessId);
        new Magento(products, magento, done.bind(this)).start();
    });
}

function addColors(magento, products, callback) {

    if (products.length > 0) {
        products.forEach(product => {
            if (product.colors.length > 0) {
                addColor(product);
            }
        });
        callback();
    } else {
        callback();
    }

    function addColor(product) {
        product.colors.forEach(color => {
            console.log(color);
            let color_sku = get_sku_v2(color.title);

            console.log(product.sku, color_sku);
            magento.catalogProductLink.assign({
                type         : 'related',
                product      : product.sku,
                linkedProduct: color_sku,
                data         : {}
            }, (err) => console.log(err));
        });

        //callback();
    }
}

function createProduct(magento, product, finish) {
    let data  = {
        category_ids     : product.category_ids,
        website_ids      : ['1'],
        price            : product.price_stock,
        special_price    : product.price_sale,
        name             : product.title,
        status           : 1,
        description      : product.description,
        short_description: product.short_description,
        visibility       : 4,
        required_options : '1',
        has_options      : '1',
        shop             : "https://worldbox.pl"
    };
    data.szes = product.sizes_table;

    magento.catalogProduct.create({
        type: 'simple',
        set : '4',
        sku : product.sku,
        data: data
    }, (err, data) => {
        if (err) {
            if (err.faultString) {
                err.faultString == 'The value of attribute "sku" must be unique' ? console.log("UPDATE") : 0;
                updateProduct(magento, product, finish);
            } else {
                logger.error('magento ', err);
                finish();
            }
        } else {
            console.log('ADD');
            product.sizes ? createOption(data, product.sizes_a) : 0;
            addImages(data, product.images);
        }
    });

    function createOption(product_id, params) {
        let sizes = [];

        params.forEach(size => {
            console.log(size);
            sizes.push({
                title     : size,
                prise     : 0,
                price_type: 'fixed'
            });
        });

        magento.catalogProductCustomOption.add({
            productId: product_id,
            data     : {
                title            : lang.sizes,
                type             : 'drop_down',
                is_require       : 1,
                sort_order       : 0,
                additional_fields: sizes
            }
        }, err => console.log('111', err));
    }

    function addImages(product_id, images) {
        images.forEach((image, index) => {
            fs.appendFileSync(file, image, 'utf8', () => {
            });
            Images.addImages({product_id: product_id, image_url: image.url}, (data) => {
                if (image.type)
                    updateImage(product_id, image);
                index == images.length - 1 ? finish() : 0;
                fs.appendFileSync(file, index + "." + images.length, 'utf8', () => {
                });
            });
        });

        function updateImage(product_id, image) {
            magento.catalogProductAttributeMedia.update({
                product  : product_id,
                file     : image.url,
                data     : {
                    types: ['image', 'small_image', 'thumbnail']
                },
                storeView: 1
            }, err => console.log(131, err));
        }
    }
}

function updateProduct(magento, product, finish, count) {
    fs.appendFileSync(file, '<-UPDATE PRODUCT->', 'utf8', () => {
    });
    !count ? count = 0 : 0;
    let sync = new Array(3);

    magentoV2.getProductInfoSku(product.sku, (err, data) => {
        if (err) {
            console.log('<error>', product.sku, count);
            console.log(err);
            console.log('</error>');
            setTimeout(() => {
                if (count < 5)
                    updateProduct(magento, product, finish, count + 1);
                else
                    finish();
            }, 1000);
        } else {
            let id = data.product_id;
            updateMain(data);
            updateImages(id);
            product.sizes_a.length > 0 ? upateSizes(id) : sync[2] = 1;
            listener();
        }
    });

    function updateMain(product_from_magento) {
        let product_id = product_from_magento.product_id;
        fs.appendFileSync(file, "<-UPDATE MAIN->" + product.price_stock + " " + product.price_sale, 'utf8', () => {
        });
        magento.catalogProduct.update({
            id  : product_id,
            data: {
                //category_ids      : [ '3', '4', '5', '7' ],
                website_ids      : ['1'],
                price            : product.price_stock,
                special_price    : product.price_sale,
                name             : product.title,
                status           : 1,
                description      : product.description,
                short_description: product.short_description,
                visibility       : 4,
                required_options : '1',
                has_options      : '1',
                szes             : product.sizes_table,
                shop             : "https://worldbox.pl",
                // category_ids      : sumArray(product_from_magento.category_ids,product.category_ids),
                category_ids     : product.category_ids,
            }
        }, err => err ? console.log(184, err) : sync[0] = 1);
    }

    function updateImages(product_id) {
        fs.appendFileSync(file, "<-UPDATE IMAGES->", 'utf8', () => {
        });
        Images.removeImages({product_id: product_id}, () => {
            product.images.forEach((image, index) => {
                Images.addImages({product_id: product_id, image_url: image.url}, data => {
                    if (image.type)
                        updateImage(product_id, image);
                    index == product.images.length - 1 ? sync[1] = 1 : 0;
                });
            });
            product.images.length == 0 ? sync[1] = 1 : 0;
        });

        function updateImage(product_id, image) {
            magento.catalogProductAttributeMedia.update({
                product  : product_id,
                file     : image.url,
                data     : {
                    types: ['image', 'small_image', 'thumbnail']
                },
                storeView: 1
            }, err => err ? console.log('208', err) : 0);
        }

    }

    function upateSizes(product_id) {
        magento.catalogProductCustomOption.list({
            productId: product_id,
        }, (err, data) => {
            data.forEach(option => {
                magento.catalogProductCustomOption.remove({
                    optionId: option.option_id
                }, () => {
                });
            });
        });

        setTimeout(() => {
            createOption(product_id, product.sizes_a);
        }, 1000);

        function createOption(product_id, params) {
            let sizes = [];

            params.forEach(size => {
                sizes.push({
                    title     : size,
                    prise     : 0,
                    price_type: 'fixed'
                });
            });

            magento.catalogProductCustomOption.add({
                productId: product_id,
                data     : {
                    title            : lang.sizes,
                    type             : 'drop_down',
                    is_require       : 1,
                    sort_order       : 0,
                    additional_fields: sizes
                }
            }, err => err ? console.log(249, sizes, err) : sync[2] = 1);
        }
    }

    function listener() {
        if (!logic.syncChecker(sync))
            setTimeout(listener, 200);
        else {
            finish();
        }
    }
}

class Magento {
    constructor(products, magento, finish) {
        this.products = products;
        this.finish   = finish;

        this.mode       = 'sync';
        this.maxThreads = 5;
        this.log        = true;
        this.magento    = magento;

        this.timeout = 60;
    }

    start() {
        let context = this;

        function ac() {
            context.addColors(context.products, context, context.finish);
        }

        let functions = [
            this.sync.bind(this),
            ac
        ];

        async.series(functions, this.finish);
    }

    async(callback) {
        //make arrays
        let context       = this;
        let productsPages = [];
        let counter       = 0;
        let productsAmout = this.products.length;
        let products      = this.products;
        let littleArray   = [];

        for (let i = 0; i < productsAmout; i++) {
            if (counter < this.maxThreads) {
                littleArray.push(products[i]);
                counter++;
            } else {
                counter     = 0;
                littleArray = [];
                productsPages.push(littleArray);
            }
        }

        if (products.length)
            r(0);

        function r(index) {
            console.log('start', index);
            context.checkOne(productsPages[index], context, () => index < productsPages.length ? r(index + 1) : callback());
        }

        fs.writeFileSync('magento-debug.json', JSON.stringify(productsPages));
        console.log('done');
    }

    sync(callback) {
        logger.info('Начинаем добвлять продукты', this.products.length);
        let context = this;

        async.eachSeries(context.products, context.checkOne.bind(context), callback);
    }

    checkOne(products, callback) {
        logger.info('проеряем продукс', products.sku);
        let context = this;
        let sync;

        if (products.price_save)
            logger.warn('Product, with sale', products.title);

        let product = products;
        let data    = {
            category_ids     : product.category_ids,
            website_ids      : ['1'],
            price            : product.price_stock,
            special_price    : product.price_save,
            name             : product.title,
            status           : 1,
            description      : product.desc,
            short_description: product.short_description,
            visibility       : 4,
            required_options : '1',
            has_options      : '1',
            shop             : "https://worldbox.pl"
        };
        data.szes   = product.sizes_table;

        logger.debug('Пробуем создать продукт');
        this.magento.catalogProduct.create({
            type: 'simple',
            set : '4',
            sku : product.sku,
            data: data
        }, (err, data) => {
            if (err && err.faultString && err.faultString.length) {
                if (err.faultString.includes('Значение атрибута "SKU" должно быть уникальным')) {
                    logger.warn('Мажента настройнная на русский язык, api Должно быть на анг');
                    context.update(product, callback, context);
                } else if (err.faultString.includes('The value of attribute "SKU" must be unique')) {
                    context.update(product, callback, context);
                } else {
                    logger.error("Ошибка", err);
                    callback(null, null);
                }
            } else {
                logger.debug('Добавили продукт продолжаем');
                try {
                    product.product_id = data;
                    context.continueAdd(product, callback, context);
                } catch (e) {
                    logger.error("Check product", data);
                    callback(null, null);
                }
            }
        });

    }

    continueAdd(product, finish, context) {
        logger.debug('Продукт поле magento API', product.product_id, product.sku);
        let done = false;

        if (product.sizes_a) {
            context.createOption(product, product.sizes_a, context, err => {
                if (err)
                    return callback(err);

                context.addImages(product, product.images, context, err => {
                    callback(err);
                });
            });
        } else {
            context.addImages(product, product.images, context, err => {
                callback(err);
            });
        }


        function callback(err) {
            if (err)
                logger.error(err);

            done = true;
            finish();
        }
    }

    // todo upload
    update(product, params, context) {
        logger.info('Обовление продукта', product.product_id, product.sku);

        let done = false;

        function getId(clb) {
            logger.debug('получем id продукта');

            magentoV2.getProductInfoSku(product.sku, (err, data) => {
                if (err)
                    return clb(err);

                logger.debug('Получили', data.product_id);
                clb(null, data.product_id);
            });
        }

        function updateMain(id, _callback) {
            logger.debug('Обновляем базовую инфу');

            if (!id)
                return _callback(new Error(`У продукта ${product.sku}`));

            product.product_id = id;
            context.updateMain(product, context, err => _callback(err, true));
        }

        function updateImages(data, _callback) {
            logger.debug('Обновляем картинки', product.product_id, product.sku);

            if (!data)
                return _callback(new Error(`У продукта ${product.sku} нету product_id`));

            if (data)
                context.updateImages(product, context, err => _callback(err, true));
        }

        function updateSizes(data, _callback) {
            logger.debug('Обновляем размеры', product.product_id, product.sku);

            if (!data || !product.sizes_a.length)
                return _callback(new Error(`У продукта ${product.product_id} нету размеров`));


            context.updateSizes(product, context, err => _callback(err, true));

        }

        function d(err) {
            err ? logger.warn(err) : logger.info(`Продукт ${product.product_id} успешно обновелн`, product.sku);

            params();
        }

        async.waterfall([
            getId.bind(context),
            updateMain.bind(context),
            updateImages.bind(context),
            updateSizes.bind(context)
        ], d);

    }

    updateSizes(product, context, finish) {
        // logger.info("magento.updateSizes start");
        context.magento.catalogProductCustomOption.list({
            productId: product.product_id,
        }, (err, data) => {
            if (err) {
                logger.error('magento.upateSizes', err);
                finish();
            } else {
                let functions = [];

                data.forEach(option => {
                    functions.push(function (callback) {
                        context.magento.catalogProductCustomOption.remove({
                            optionId: option.option_id
                        }, () => callback());
                    });
                });

                function done() {
                    // logger.info("magetno.updateSizes done");
                    createOption(product.product_id, product.sizes_a);
                }

                async.series(functions, done);
            }
        });


        function createOption(product_id, params) {
            let sizes = [];

            params.forEach(size => {
                sizes.push({
                    title     : size,
                    prise     : 0,
                    price_type: 'fixed'
                });
            });

            context.magento.catalogProductCustomOption.add({
                productId: product_id,
                data     : {
                    title            : lang.sizes,
                    type             : 'drop_down',
                    is_require       : 1,
                    sort_order       : 0,
                    additional_fields: sizes
                }
            }, err => err ? console.log(489, err) : finish());
        }

    }

    updateMain(product, context, callback) {
        // logger.info('magento.updateMain start', product.title);

        let product_id = product.product_id;
        context.magento.catalogProduct.update({
            id  : product_id,
            data: {
                //category_ids      : [ '3', '4', '5', '7' ],
                website_ids      : ['1'],
                price            : product.price_stock,
                special_price    : product.price_save,
                name             : product.title,
                status           : 1,
                description      : product.description,
                short_description: product.short_description,
                visibility       : 4,
                required_options : '1',
                has_options      : '1',
                szes             : product.sizes_table,
                shop             : "https://worldbox.pl",
                // category_ids      : sumArray(product_from_magento.category_ids,product.category_ids),
                category_ids     : product.category_ids,
            }
        }, callback);
    }

    /**/
    createOption(product, params, context, callback) {
        logger.debug('Добавляем размеры', product.product_id, product.sku);
        let sizes = [];

        params.forEach(size => {
            sizes.push({
                title     : size,
                prise     : 0,
                price_type: 'fixed'
            });
        });

        context.magento.catalogProductCustomOption.add({
            productId: product.product_id,
            data     : {
                title            : lang.sizes,
                type             : 'drop_down',
                is_require       : 1,
                sort_order       : 0,
                additional_fields: sizes
            }
        }, err => {
            if (err) {
                logger.error('Ошибка при добавлении размеров', err);
                return clb(err);
            }

            callback();
        });
    }

    addImages(product, images, context, finish) {
        logger.debug('Добавляем катинки', product.sku);

        if (!images.length) {
            logger.warn('У продукта нету изображений', product.sku);
            return finish();
        }
        let index      = 0;
        const addImage = (image, clb) => {
            logger.debug('Путь картинки', image.path);

            const imageInBase64 = base64.base64Sync(image.path) || '';
            const fixedImage    = imageInBase64 ? imageInBase64.substr('data:image/jpg;base64,'.length) : '';

            const types = ['thumbnail'];

            if (!index) {
                types.push('small_image');
                types.push('image');
            }

            context.magento.catalogProductAttributeMedia.create({
                product  : product.product_id,
                storeView: 1,
                data     : {
                    file: {
                        content: fixedImage,
                        mime   : 'image/jpeg'
                    },
                    types
                }
            }, err => {
                err && logger.error(err);
                index++;
                clb();
            });
        };

        async.eachSeries(images, addImage.bind(this), err => {
            err && logger.warn(err);

            logger.info(err ? 'Не добавленно' : 'Добавленно без ошибок');

            finish();
        });


        // function updateImage(product_id, image) {
        //     context.magento.catalogProductAttributeMedia.update({
        //         product  : product_id,
        //         file     : image.url,
        //         data     : {
        //             types: ['image', 'small_image', 'thumbnail']
        //         },
        //         storeView: 1
        //     }, err => console.log(err));
        // }
    }

    updateImages(product, context, finish) {
        // logger.info('magento.updateImages update images');

        return finish();

        // if (product.images.length > 0) {
        //     logger.debug('Обновляем картинки у', product.sku);
        //     logger.debug('Удаляем картинки у', product.sku);
        //     Images.removeImages({product_id: product.product_id}, err => {
        //         if (err) {
        //             logger.error(err);
        //             return finish();
        //         }
        //
        //         logger.debug('Удалили идем дальше');
        //
        //         const addImage = (image, clb) => {
        //             logger.debug('Добавляем изображение', image.url);
        //
        //             Images.addImages({product_id: product.product_id, image_url: image.url}, err => {
        //                 if (err)
        //                     return clb(err);
        //
        //                 if (!image.index)
        //                     updateImage(product.product_id, image);
        //
        //                 clb(null, true);
        //             });
        //         };
        //
        //         async.each(product.images, addImage.bind(this), (err) => {
        //             err && logger.warn(err);
        //
        //             finish();
        //         });
        //     });
        // } else {
        //     logger.warn('У продукта нету изображений, скипаем', product.sku);
        //     finish();
        // }
        //
        // function updateImage(product_id, image) {
        //     context.magento.catalogProductAttributeMedia.update({
        //         product  : product_id,
        //         file     : image.url,
        //         data     : {
        //             types: ['image', 'small_image', 'thumbnail']
        //         },
        //         storeView: 1
        //     }, err => err ? console.log('208', err) : 0);
        // }

    }

    addColors(products, context, callback) {

        if (products.length > 0) {
            try {
                products.forEach(product => {
                    if (product.colors.length > 0) {
                        addColor(product);
                    }
                });
            } catch (e) {
                logger.error(e);
            }
            callback();
        } else {
            callback();
        }

        function addColor(product) {
            product.colors.forEach(color => {
                // logger.info('add colors', color.title);
                let color_sku = get_sku_v2(color.title);

                console.log(product.sku, color_sku);
                context.magento.catalogProductLink.assign({
                    type         : 'related',
                    product      : product.sku,
                    linkedProduct: color_sku,
                    data         : {}
                }, (err) => logger.warn('product not found'));
            });
        }
    }
}


class Product {
    constructor(product) {
        this.product = product;
    }

    getSizeTable() {

    }
}


function createTestProduct(magento, params) {
    let images = params.images;
    let sizes  = params.sizes;
    magento.catalogProduct.create({
        type: 'simple',
        set : '4',
        sku : 'FINAL 10',
        data: {
            category_ids     : ['3', '4', '5', '7'],
            website_ids      : ['1'],
            price            : '100.0000',
            special_price    : '99.9000',
            name             : "FINAL 10",
            status           : 1,
            description      : "Ths is my description",
            short_description: "this is me",
            visibility       : 4,
            required_options : '1',
            has_options      : '1',
            szes             : generateSizeTable(),
            shop             : "https://worldbox.pl"
        }
    }, (err, data) => {
        if (err) throw err;
        console.log(err, data);
        createOption(data, [[24, 36, 41, 42, 45], [1, 2, 3, 4, 5]]);
        addImages(data, [{type: 1, url: '/c/a/cam00681.jpg'}, {url: "/c/a/cam00585_1.jpg"}]);
    });


    function createOption(product_id, params) {
        // params = array of sizes
        let sizes = [];
        params.forEach(size => {
            let titleStr = "";
            size.size && !size.size_eu ? titleStr += "Раземр: " + size.size + " " : 0;
            size.size_eu ? titleStr += "EU: " + size.size_eu + " " : 0;
            size.size_us ? titleStr += "US: " + size.size_us + " " : 0;
            size.size_uk ? titleStr += "UK: " + size.size_uk + " " : 0;
            size.size_cm ? titleStr += "CM: " + size.size_cm + " " : 0;
            sizes.push({
                title     : titleStr,
                prise     : 0,
                price_type: 'fixed'
            });
        });

        magento.catalogProductCustomOption.add({
            productId: product_id,
            data     : {
                title            : lang.sizes,
                type             : 'drop_down',
                is_require       : 1,
                sort_order       : 0,
                additional_fields: sizes
            }
        }, err => console.log('323', err));
    }

    function addImages(product_id, images) {
        images.forEach(image => {
            console.log(image);
            Images.addImages({product_id: product_id, image_url: image.url}, data => {
                if (image.type)
                    updateImage(product_id, image);
            });
        });

        function updateImage(product_id, image) {
            magento.catalogProductAttributeMedia.update({
                product  : product_id,
                file     : image.url,
                data     : {
                    types: ['image', 'small_image', 'thumbnail']
                },
                storeView: 1
            }, err => console.log('342', err));
        }
    }
}

function getMedia(magento) {
    magento.catalogProductAttributeMedia.list({
        product: 2,
    }, (err, data) => console.log(data));
}

function optionsList(magento, id) {
    magento.catalogProductCustomOption.list({
        productId: id,
    }, (err, data) => {
        data.forEach(option => {
            magento.catalogProductCustomOption.info({
                optionId: option.option_id,
            }, (err, data) => console.log(err, data));
        });
    });
}

function getProducts(magento) {
    magento.catalogProduct.list((err, data) => {
        // console.log(data);
        data.forEach(product => {
            magento.catalogProduct.info({id: product.product_id}, (err, product_info) => {
                console.log(product_info);
            });
        });
    });
}

function listStores(magento) {
    magento.store.list((err, data) => console.log(err, data));
}

function generateSizeTable(sizes) {
    let out = '<table align="center" border="0" style="width:511px;height:102px;"><tbody>';
    if (sizes.length > 0)
        if (sizes[0].size_us)
            out += '<tr><td>US</td><td>UK</td><td>EU</td><td>CM</td></tr>';
    sizes.forEach(size => {
        out += '<tr>';
        size.size && !size.size_us ? out += '<td>' + size.size + '</td>' :
            out += '<tr><td>' + size.size_us + '</td><td>' + size.size_uk + '</td><td>' + size.size_eu + '</td><td>' + size.size_cm + '</td>';
        out += '</tr>';
    });
    out += '</tbody></table>';
    return out;
}

function get_sku(link) {
    let color_sku = link.substr(link.lastIndexOf('-') + 1);
    if (color_sku.length <= 3) {
        color_sku = link.split('-');
        color_sku = color_sku[color_sku.length - 2] + "-" + color_sku[color_sku.length - 1].substr(0, color_sku[color_sku.length - 1].indexOf('.'));
    } else {
        color_sku = color_sku.substr(0, color_sku.indexOf('.'));
    }
    return color_sku;
}

function get_sku_v2(title) {
    let sku = title.substr(title.indexOf("(") + 1);
    return sku.substr(0, sku.lastIndexOf(")"));
}

module.exports.auth   = auth;
module.exports.addAll = startAddProducts;