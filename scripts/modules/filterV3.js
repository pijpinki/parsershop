let fs     = require('fs');
let async  = require('async');
let logger = require('./loger');

let ProductDB = require('../database/producs');

class Filter {
    constructor(products, callback) {
        this.products = products || fs.readFileSync('./data/products.json');
        this.callback = callback;

        this.finishProducts = [];
        this.product        = {};
        this.same           = true;
    }

    findProduct(callback) {
        ProductDB.get({number: this.product.sku}, (err, producs) => {
            if (err)
                return callback(err);

            if (producs.length > 0) {
                this.product.type = "update";
                this.product.dbId = producs[0].id;
                callback();
            } else {
                this.same         = false;
                this.product.type = "new";
                this.product.images.length > 0 ? this.finishProducts.push(this.product) : 0;
                callback(null, "");
            }
        });
    }

    checkMainParams(callback) {
        if (!this.same)
            return callback(null, 'next');


        ProductDB.get({number: this.product.sku}, (err, products) => {
            if (err)
                return callback(err);

            let product = products[0];
            let error   = false;

            if (product.name !== this.product.name) {
                logger.debug('!= name ', product.name, this.product.name);
                error = true;
            }
            if (product.unit_price !== this.product.price) {
                logger.debug('!= price', product.unit_price, this.product.price);
                error = true;
            }
            if (product.unit_sale_price !== this.product.sale) {
                logger.debug('!= sale', product.unit_price, this.product.sale);
                error = true;
            }

            if (error) {
                this.same         = false;
                this.product.type = "update";
                this.finishProducts.push(this.product);
                callback(null, "added");
            } else {
                callback(null, true);
            }
        });

    }

    checkPhotos(callback) {
        if (!this.same)
            callback(null, 'next');
        else {
            callback(null, 'next');
        }
    }

    checkColors(callback) {
        if (!this.same)
            callback(null, "next");
        else {
            callback(null, "next");
        }
    }

    start() {
        let functions = [];

        if (this.products.length > 0) {
            checkOne.call(this, 0);
        } else {
            this.callback(null, "not found");
        }

        function checkOne(index) {
            if (index < this.products.length) {
                this.product = this.products[index];

                functions = [
                    this.findProduct.bind(this),
                    this.checkMainParams.bind(this),
                    this.checkPhotos.bind(this),
                    this.checkColors.bind(this)
                ];

                function next(err) {
                    err ? logger.error(err) : logger.info('Yep');

                    this.same = true;

                    checkOne.call(this, index + 1);
                }

                async.series(functions, next.bind(this));
            } else {
                this.callback(null, this.finishProducts);
            }
        }
    }
}

module.exports = Filter;