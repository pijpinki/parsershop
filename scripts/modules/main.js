/**
 * Created by pinki on 02.06.2017.
 */
function syncChecker(array) {
    var done = false;

    for(var i = 0; i < array.length; i++){
        if(array[i])
            done = true;
        else{
            done = false;
            break;
        }
    }

    return done;
}

function fillArray(array, el) {
    for(let i = 0; i < array.length; i++){
        array[i] = el;
    }
    return array;
}

function findInObjectArray(array,val,key) {
    var data = false;
    for(var i = 0; i < array.length; i++){
        var item = array[i];

        if(item[key] == val){
            data = item;
            break;
        }
    }
    return data;
}

function fillError(error, code, message) {
    error.status   = true;
    error.message += " "+message+" ";
    error.code     = code;
}

function sendData(res, code, data) {
    res.send({code:code, data:data});
}

function sendError(res, error) {
    res.send(error);
}

function createError() {
    return {code:200, message:"", status:false};
}

function auth(token, callback) {
    let Users = require('../database/users');

    Users.getUserByToken({token:token}, users => {
        users.length > 0 ? callback(users[0]) : callback(false);
    });
}

function auth2 (token, callback) {
    let Users = require('../database/users');
    let error = false;

    !token ? error = true : error = false;

    if(!error){
        Users.getUserByToken({token:token}, users => {
            error = createError();
            if(users.length > 0)
                callback(error,users[0]);
            else {
                fillError(error, 403, "Access dennie");
                callback(error,false);
            }
        });
    }else {
        fillError(error,503, "No token");
        callback(error,false);
    }

}

function auth3(token, success, error) {
    auth2(token, (err, user) =>{
        if(err.status)
            error();
        else {
            success(user);
        }
    });
}
function sumArray(array1, array2) {
    let out = [];
    let same, tmp;

    if(array1.length < array2.length){
        tmp    = array2;
        array1 = array2;
        array2 = tmp;
    }

    array1.forEach((item1,i) => {
        same = false;
        for(let j = 0; j < array2.length; j ++){
            if(item1 == array2[j]){
                out.push(array2[j]);
                same = true;
                break;
            }
        }
        if(!same)
            out.push(item1);
    });

    return out;
}

module.exports.auth              = auth;
module.exports.fillError         = fillError;
module.exports.createError       = createError;
module.exports.sendError         = sendError;
module.exports.sendData          = sendData;
module.exports.syncChecker       = syncChecker;
module.exports.findInObjectArray = findInObjectArray;
module.exports.auth2             = auth2;
module.exports.auth3             = auth3;
module.exports.fillArray         = fillArray;
module.exports.summary           = sumArray;