/**
 * Created by pinki on 25.06.2017.
 */

let MagentoAPI = require('magento');
let config     = require('../config/magento');

let magento = new MagentoAPI({
    host : config.siteBase,
    port : 80,
    path : config.apiPath,
    login: config.user,
    pass : config.password
});


class magentoV2 {
    constructor(){
        magento.login(function(err, sessId) {
            !err ? console.log("Magento login success !", sessId) : console.log('Magento login error', err);
        });
        this.magento = magento;
    }
    getProductInfoSku(sku, callback){
        this.magento.catalogProduct.info({
            id             : sku,
            storeView      : 1,
            attributes     : 1,
            identifierType :"SKU"
        }, callback);
    }
    getProductInfoId(){

    }

    getAllProducts(callback){
        magento.catalogProduct.list(callback);
    }

    deleteProduct(product, callback){
        console.log(product);
        magento.catalogProduct.delete({
            id: product.product_id
        }, callback);
    }

    deleteAllProducts(s){
        let context = this;
        let products= [];

        this.getAllProducts((err, products_d) =>{
            if(err) throw err;
            products = products_d;
            s ? sync(): async();
        });


        function async() {
            console.log('delete all async');
            products.forEach((product)=>{
                context.deleteProduct(product, (err)=>{ if(err){console.log(err); throw err}});
            });
        }

        function sync(index) {
            console.log('delete all sync',index);
            if(!index) index = 0;

            context.deleteProduct(products[index],(err) => {
                if(err) throw err;

                if(index < products.length -1)
                    sync(index + 1);
            });

        }
    }

    getOptionList(){
/*        magento.catalogProduct.info({
            id: 5384
        }, (err, data)=>console.log(data));*/
/*        magento.catalogProductAttribute.list({
            productId:  4,
        }, (err, data) => {
            console.log(data);
            data.forEach(option => {
                magento.catalogProductCustomOption.info({
                    optionId:   option.option_id,
                }, (err,data)=>console.log(err, data));
            });
        });*/
        magento.catalogProductLink.list({
            type:     'related',
            product:  'BA5351-009'
        }, (err,data)=>console.log(err,data));

    }
}


module.exports  = magentoV2;