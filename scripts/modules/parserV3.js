let cheerio  = require('cheerio');
let needle   = require('needle');
let async    = require('async');
let fs       = require('fs');
let config   = require('../config/main').options;
const Prices = require('./prices');
// let window   = {};

class parserV3 {
    constructor(html, type) {
        this.$    = cheerio.load(html);
        this.type = type;
        this.html = html;
    }

    /**
     * Returns array of links
     */
    getLinks() {
        let _this = this;
        let links = [];
        this.$(".products").find('.row.filter-container').children().each((index, item) => {
            links.push(_this.$(item).find('.products__item-name').find('a').attr('href'));
        });
        return links;
    }

    /**
     * Return full product
     */
    getProduct(callback) {
        let _this   = this;
        let product = {};

        product.id          = _this.getID();
        product.sku         = _this.getSKU();
        product.name        = _this.getName();
        product.title       = _this.getName();
        product.price       = _this.getPrice();
        product.sale        = _this.getSale();
        product.description = _this.getDescription();
        product.sizes_table = _this.getSizesTable();
        product.sizes       = _this.getSizesA();
        product.sizes_a     = _this.getSizesLine();
        product.desc        = _this.getDescription();

        product.price_stock = product.price;
        product.price_save  = product.sale;

        async.parallel({colors: _this.getColors.bind(_this), images: _this.getImages.bind(_this)}, fillProducs);

        function fillProducs(err, data) {
            product.colors = data.colors;
            product.images = data.images;
            callback(err, product);
        }
    }

    getName() {
        let name = this.$('.product__name').find('h1').text();
        name     = name.replaceAll("Buty ", "");
        name     = name.replaceAll("CZAPKA ", "");
        name     = name.replaceAll("SPODNIE ");
        name     = name.replaceAll("KURTKA ", "");
        name     = name.replaceAll("BLUZA ", "");
        return name;
    }

    getPrice() {
        let price = this.$('.product__price').find('.product__price_shop').html();

        price = price ? price.replace(",", "") : 0;
        price = price ? parseFloat(price) : 0;

        return Prices.getPrice(price);
    }

    getSale() {
        let sale = this.$('.product__price').find('.product__price_producent').html();
        sale ? sale = sale.replace(",", "") : 0;
        sale ? sale = parseFloat(sale) : 0;

        return Prices.getPrice(sale);
    }

    getDescription() {
        return this.$('.col-md-12.col-sm-12.col-xs-12.description').find('.col-md-12.product__description').html();
    }

    getSizesTable() {
        return this.$('.col-md-12.product__size_table').html();
    }

    getSKU() {
        let name = this.$('.product__name').find('h1').text();
        name     = name.substring(name.indexOf('(') + 1);
        name     = name.substring(0, name.indexOf(')'));
        return name;
    }

    getSizesA() {
        let _this = this;
        let sizes = [];

        this.$('.product__size').find('ul').children().each((index, item) => {
            item = _this.$(item);

            const obj = {};

            const size    = item.attr('data-value');
            const size_us = item.attr('data-sizeus');
            const size_uk = item.attr('data-sizeuk');
            const size_cm = item.attr('data-sizecm');
            const size_eu = item.attr('data-sizeeu');

            size ? obj.size = size : null;
            size_cm ? obj.size_cm = size_cm : null;
            size_uk ? obj.size_uk = size_us : null;
            size_us ? obj.size_us = size_us : null;
            size_eu ? obj.size_eu = size_eu : null;

            sizes.push(obj);
        });
        return sizes;
    }

    getImages(callback) {
        this.productPhotos$ = this.$('.owl-carousel').first();
        let images          = [];
        let context         = this;
        this.productPhotos$.children('div').each((index, el) =>
            images.push(context.$(el).children('img').attr('src')));

        async.map(images, this.downloadImages, done);

        function done(err, images) {
            if (err)
                callback(null, []);
            else
                callback(err, images);
        }

    }

    downloadImages(image_url, callback) {
        if (image_url) {
            let image_name = image_url.substr(image_url.lastIndexOf('/') + 1);
            let image_path = config.photos_path + image_name;
            let out        = fs.createWriteStream(image_path);
            needle.get(image_url).pipe(out).on('finish', finish.bind({image_name: image_name}));
        } else {
            callback(true, null);
        }

        function finish() {
            callback(null, {
                url : config.photos_site_path + this.image_name,
                path: config.photos_path + this.image_name
            });
        }
    }

    getColors(_callback) {
        // let _this    = this;
        // let products = [];
        let id       = this.getID();

        let params = {"products": id.toString()};

        let getDataSrc = "https://qai-ir.quartic.pl/Ai/lbb1?qparams=" + new Buffer(JSON.stringify(params)).toString('base64');

        function getDataScript(callback) {
            needle.get(getDataSrc, (err, res) => {
                if (err) {
                    callback(err);
                } else {
                    let links = res.body.substring(res.body.indexOf("_qS.src = "));
                    links     = links.substring(0, links.indexOf(";") - 1);
                    links     = links.substring("_qS.src = ".length + 1);
                    links     = "https:" + links;
                    callback(null, links);
                }
            });
        }

        function getData(script, callback) {
            if (script) {
                needle.get(script, (err, res) => {
                    if (err) {
                        callback(err, null);
                    } else {
                        let body = res.body.toString();
                        let reco = body.substring(body.indexOf("var reko = "));

                        reco = reco.substring(0, reco.indexOf("];") + 1);
                        reco = reco.substring("var reko = ".length);
                        reco = reco.replaceAll("protocol", "https");
                        reco = reco.replaceAll("\"+", "");
                        reco = reco.replaceAll("+\"", "");
                        reco = JSON.parse(reco);
                        callback(null, reco);
                    }
                });
            } else {
                callback("no script", null);
            }
        }

        function done(err, data) {
            if (err)
                _callback(err, null);
            else {
                _callback(null, data);
            }
        }


        async.waterfall([getDataScript, getData], done);

        // https://qsdw1-ir.quartic.pl/creation/snippetHtml?customer=d366a6fe3d717ae1&slot=rtb_15490&creation=connect_15490_to_5801&width=800&height=200&cb=fc0ae&clickTag=&ci=58767&v=h_8&qdpi=lbb1&qrID=ad_5988bdd02d7d5&dm=WyJQcm9kdWt0eSIsIjgwMHgyMDAiLCIiLCIiXQ==&qparams=eyJwIjoiODgzNDUifQ==
    }

    getID() {
        let varieble = this.html.substring(this.html.indexOf('pid') + 7);
        varieble     = varieble.substring(0, varieble.indexOf(",") - 1);
        varieble     = parseInt(varieble);
        return varieble;
    }

    getSizesLine() {
        let sizesLines = [];

        let tab = '&#09;';

        const sizezObj = this.getSizesA();

        sizezObj.forEach(size => {
            sizesLines.push(size.size_cm + " CM" + tab + size.size_uk + " UK" + tab + size.size_us + " US" + tab + size.size_eu + " EU");
        });

        return sizesLines;
    }

}

String.prototype.replaceAll = function (search, replacement) {
    const target = this;
    return target.split(search).join(replacement);
};

module.exports = parserV3;

