/**
 * Created by pinki on 02.06.2017.
 */
let login = {
    login : function () {
        let error = false;
        let login = $('#login').val();
        let pass  = $('#pass').val();

        if(!login){
            $('#error').html(strings.main.EMPTY_USER);
            error = true;
        }
        if(!pass){
            $('#error').html(strings.main.EMPTY_PASS);
            error = true;
        }
        if(!error)
            api.user.login({login:login, pass:pass}, done);
        
        function done(res) {
            if(res.code == 200){
                location.href = '/';
            }else {
                $('#error').html(strings.main.WRONG_LOGIN);
            }
        }
    }
};