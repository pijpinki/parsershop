/**
 * Created by pinki on 02.06.2017.
 */
let api = {
    user : {
        login : function (params, callback) {
            $.ajax({
                type     : "GET",
                url      : '/api/user',
                data     : params,
                dataType : 'json',
                success  : callback
            });
        }
    },
    parser : {
        start       : function (params, callback) {
            $.ajax({
                type     : "POST",
                url      : '/api/parser/control',
                data     : params, // token
                dataType : 'json',
                success  : callback
            });
        },
        getStatus   : function (params, callback) {
            $.ajax({
                type     : "GET",
                url      : '/api/parser/control',
                data     : params, // token, current status [0 <> 1]
                dataType : 'json',
                success  : callback,
                error    : repeat
            });
            function repeat() {
                api.parser.getStatus(params,callback);
            }
        },
        getInfo     : function (params, callback) {
            $.ajax({
                type     : "GET",
                url      : '/api/parser/info',
                data     : params, // token
                dataType : 'json',
                success  : callback
            });
        },
        getLogs     : function (params, callback) {
            $.ajax({
                type     : "GET",
                url      : '/api/parser/log',
                data     : params, // token
                dataType : 'json',
                success  : callback
            });
        },
        resetStatus : function (params, callback) {
            $.ajax({
                type     : "PUT",
                url      : '/api/parser/control',
                data     : params, // token
                dataType : 'json',
                success  : callback
            });
        }
    },
    products : {
        getNew     : (params,callback)=>{
            $.ajax({
                type     : "GET",
                url      : '/api/products/new',
                data     : params, // token
                dataType : 'json',
                success  : callback
            });
        },
        getUpdated : (params,callback)=>{
            $.ajax({
                type     : "GET",
                url      : '/api/products/updated',
                data     : params, // token
                dataType : 'json',
                success  : callback
            });
        }
    }
};