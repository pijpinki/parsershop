/**
 * Created by pinki on 02.06.2017.
 */
let ids = {
    panel : {
        status : {
            card    : "#status-card",
            title   : "#status-title",
            error   : "#status-error",
            text    : "#status-text",
            buttons : {
                status : '#status-status-button',
                start  : '#status-start-button',
                reset  : '#status-reset-button'
            }
        },
        added : {
            card    : "#added-card",
            title   : "#added-title",
            error   : "#added-error",
            text    : "#added-text",
            chacked : "#chacked-text",
            buttons : {
                seeAll : "#added-seeall-button"
            }
        },
        updated : {
            card    : "#updated-card",
            title   : "#updated-title",
            error   : "#updated-error",
            text    : "#updated-text",
            buttons : {
                seeAll : "#updated-seeall-button"
            }
        },

        mainList : {
            card : "#main-list-card",
            ul   : "#main-list-ul"
        },

        dialog : {
            main  : "#dialog",
            title : "#dialog-title",
            text  : "#dialog-text",
            buttons : {
                accept : "#dialog-accept",
                dennie : "#dialog-dennie"
            }
        }
    }
};