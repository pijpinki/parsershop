/**
 * Created by pinki on 02.06.2017.
 */
let strings = {
    main : {
        EMPTY_PASS  : "",
        EMPTY_USER  : "",
        WRONG_LOGIN : "",

        // reset warring
        RESET_TITLE : "",
        RESET_TEXT  : "",
        panel : {
            status : {
                title   : "",
                error   : "",
                text    : "",
                buttons : {
                    status : ["",""],
                    start  : "",
                    reset  : ""
                }
            },
            updated : {
                title   : "",
                error   : "",
                text    : "",
                buttons : {
                    seeAll : ""
                }
            },
            added  : {
                title   : "",
                error   : "",
                text    : "",
                chacked : "",
                buttons : {
                    seeAll : ""
                }
            },
            dialog : {
                title : "",
                text  : "",
                buttons : {
                    accept : "",
                    dennie : ""
                }
            }
        }
    },
    ru   : {
        EMPTY_PASS  : "Введите пароль",
        EMPTY_USER  : "Введите Логин",
        WRONG_LOGIN : "Неверные данные",

        // reset warring
        RESET_TITLE : "Внимание",
        RESET_TEXT  : "Копка сброса предназначена  на случай повисания парсера и вывода его с этого режима" +
        " Принудительный сброс статуса может повлиять на работоспособность парсера, крайне не советуется  нажимать на эту" +
        " проклятую кнопку",

        panel : {
            status : {
                title : "Статус",
                error : "",
                text  : "До сделующего запуска: ",
                buttons : {
                    status : ["Ожидает","Запущен"],
                    start  : "Запустить",
                    reset  : "СБРОСИТЬ"
                }
            },
            updated : {
                title : "Обновленно",
                error : "",
                text  : "товаров",
                buttons : {
                    seeAll : "Просмотреть"
                }
            },
            added  : {
                title   : "Статистика",
                error   : "",
                text    : "Дабавленно",
                chacked : "Спарсено",
                buttons : {
                    seeAll : "Прсмотреть"
                }
            },

            dialog : {
                title : "",
                text  : "",
                buttons : {
                    accept : "Принять",
                    dennie : "Отмена"
                }
            }
        }
    },

    applyPanel : function () {
        $(ids.panel.added.title).html(strings.main.panel.added.title);
        $(ids.panel.added.text).html(strings.main.panel.added.text);
        $(ids.panel.added.buttons.seeAll).html(strings.main.panel.added.buttons.seeAll);

        $(ids.panel.status.title).html(strings.main.panel.status.title);
        $(ids.panel.status.text).html(strings.main.panel.status.text);
        $(ids.panel.status.buttons.status).html(strings.main.panel.status.buttons.status[0]);
        $(ids.panel.status.buttons.start).html(strings.main.panel.status.buttons.start);
        $(ids.panel.status.buttons.reset).html(strings.main.panel.status.buttons.reset);

        $(ids.panel.updated.title).html(strings.main.panel.updated.title);
        $(ids.panel.updated.text).html(strings.main.panel.updated.text);
        $(ids.panel.updated.buttons.seeAll).html(strings.main.panel.updated.buttons.seeAll);

        $(ids.panel.dialog.buttons.accept).html(strings.main.panel.updated.buttons.accept);
        $(ids.panel.dialog.buttons.dennie).html(strings.main.panel.updated.buttons.dennie);
    }
};

function applyLang(lang) {
    strings.main = strings[lang];
}

applyLang('ru');