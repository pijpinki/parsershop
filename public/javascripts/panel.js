/**
 * Created by pinki on 02.06.2017.
 */
let panel = {
    token  : getCookie('token'),
    status : 0,

    main  : function () {
        strings.applyPanel();

        panel.statusActivity();
        panel.addedActivity();
        panel.updatedActivity();
        panel.logActivity();
    },

    statusActivity  : function () {
        let counter = 100;

        let statusButton = $(ids.panel.status.buttons.status);
        let startButton  = $(ids.panel.status.buttons.start);
        let resetButton  = $(ids.panel.status.buttons.reset);

        changeStatusButton(panel.status);

        startButton.on('click', onClickStart);
        resetButton.on('click', onClickReset);

        longPull();
        resetButtonLogic();
        api.parser.getInfo({token:panel.token},time);

        function time(chat) {
            let localTime = new Date();
            let chatTime  = chat.data.end_time;
            chatTime  = new Date(Date.parse(chatTime));
            localTime = localTime.getTime();
            chatTime  = chatTime.getTime() + 86400 * 1000;

             let remeaning = chatTime - localTime;
             remeaning /= 1000;
             remeaning = Math.round(remeaning)+"";

            $(ids.panel.status.text).html(strings.main.panel.status.text + remeaning.toHHMMSS());

            setTimeout(()=>{
                if(counter > 0){
                    counter --;
                    time(chat);
                }else {
                    api.parser.getInfo({token:panel.token},time);
                    counter = 100;
                }

            },1000)
        }

        function onClickStart() {
            api.parser.start({token:panel.token}, data =>{
                if(data.code == 200){
                    changeStatusButton(0);
                }else
                    changeStatusButton(1);
            })
        }

        function onClickReset() {
            // show alert modal
            panel.render.dialog.show(
                strings.main.RESET_TITLE, strings.main.RESET_TEXT,
                {accept : () => api.parser.resetStatus({token:panel.token},()=>{})}
            );
        }

        function longPull() {
            api.parser.getStatus({token:panel.token, status:panel.status}, data =>{
                if(data.code == 200){
                    panel.status = data.data.status;
                    changeStatusButton(panel.status);
                    panel.updateUpdated();
                    panel.updateAdded();
                    panel.updateLogs();
                    resetButtonLogic();
                    longPull();
                }else {
                    longPull();
                }
            });
        }

        function changeStatusButton(status) {
            if(status == 1)
                statusButton.css('background','red');
            else
                statusButton.css('background','green');
            statusButton.html(strings.main.panel.status.buttons.status[status]);
        }

        function resetButtonLogic() {
            if(panel.status == 1)
                resetButton.css('display','block');
            else
                resetButton.css('display','none');
        }
    },

    addedActivity   : function () {
        let moreButton = $(ids.panel.added.buttons.seeAll);

        moreButton.on('click', seeAll);

        function seeAll() {
            api.products.getNew({token:panel.token}, (data)=>{
                if(data.code==200){
                    console.log(data);
                    panel.render.logs.clear();
                    data.data.products.forEach(product => panel.render.logs.addOne(product));
                }
            })
        }

        this.updateAdded();
    },

    updatedActivity : function () {
        let moreButton = $(ids.panel.updated.buttons.seeAll);

        moreButton.on('click', seeAll);

        function seeAll() {
            api.products.getUpdated({token:panel.token}, (data)=>{
                if(data.code==200){
                    console.log(data);
                    panel.render.logs.clear();
                    data.data.products.forEach(product => panel.render.logs.addOne(product));
                }
            })
        }

        this.updateAdded();
        this.updateUpdated();
    },

    logActivity     : function () {
        panel.updateLogs();
    },

    updateAdded     : function () {
        api.parser.getInfo({token:panel.token}, data => {
            if(data.code == 200){
                change(data.data.last_added, data.data.chacked);
            }
        });

        function change(added, chacked) {
            $(ids.panel.added.text).html(strings.main.panel.added.text + " " + added);
            $(ids.panel.added.chacked).html(strings.main.panel.added.chacked + " " + chacked);
        }
    },

    updateUpdated   : function () {
        api.parser.getInfo({token:panel.token}, data => {
            if(data.code == 200){
                change(data.data.last_updated)
            }
        });

        function change(updated) {
            $(ids.panel.updated.text).html(strings.main.panel.updated.text + " " + updated);
        }
    },

    updateLogs      : function () {
        api.parser.getLogs({token:panel.token}, data => {
            if(data.code == 200){
                panel.render.logs.clear();

                let logs = data.data.logs;
                logs.forEach(log => panel.render.logs.addOne({},log));
            }
        })
    },

    render : {
        logs : {
            clear : () => {
                $(ids.panel.mainList.ul).html("");
            },
            addOne : (product,log) =>{
                if(log) {
                    let h = "" +
                        "<li class=\"mdc-list-item table-main\" style='display:inherit;'>" +
                            "<span class='table-left'>Added: " + log.added + " </span>" +
                            "<span class='table-center'>Updated: " + log.updated + " </span>" +
                            "<span class='table-chacked'>Checked: " + log.chacked + " </span>" +
                            "<span class='table-right'>Date: " + log.date + " </span>" +
                        "</li>";
                    $(ids.panel.mainList.ul).append(h);
                }else {
                    let h =
                        "<li class=\"mdc-list-item table-main product-li    \" style='display:inherit;'>" +
                            "<img class='mdc-list-item__start-detail' src='"+product.product_image_url+"' width='56' height='56'/>"+
                            "<span>"+product.product_code+" </span>"+
                            "<span>"+product.name+" </span>"+
                            "<span class='price'>"+product.unit_price+" </span>"+
                            "<span class='price-sale'>"+product.unit_sale_price+" </span>"+
                        "</li>"
                    $(ids.panel.mainList.ul).append(h);
                }
            }
        },
        dialog : {
            show : (title, text, functions) => {
                let dialog = new mdc.dialog.MDCDialog(document.querySelector(ids.panel.dialog.main));
                dialog.show();
                $(ids.panel.dialog.main).css('display','inline-grid');
                $(ids.panel.dialog.title).html(title);
                $(ids.panel.dialog.text).html(text);

                if(functions){
                    $(ids.panel.dialog.buttons.accept).on('click', functions.accept);
                    if(functions.dennie)
                        $(ids.panel.dialog.buttons.dennie).on('click', functions.dennie);
                    else
                        $(ids.panel.dialog.buttons.dennie).on('click', ()=> {$(ids.panel.dialog.main).css('display','none');});
                }
            }
        }
    }

};

String.prototype.toHHMMSS = function () {
    let sec_num = parseInt(this, 10); // don't forget the second param
    let hours   = Math.floor(sec_num / 3600);
    let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
};

function getCookie (name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}