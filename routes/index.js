var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    let token = req.cookies.token;
    if(token){
      res.render('index', { title: 'Express' });
    }else {
      res.redirect('/login');
    }

});

module.exports = router;
