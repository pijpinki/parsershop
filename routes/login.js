/**
 * Created by pinki on 02.06.2017.
 */
let express = require('express');
let router  = express.Router();

router.get('/', function (req, res) {
   res.render('login');
});

module.exports = router;