/**
 * Created by pinki on 02.06.2017.
 */
let express      = require('express');
let router       = express.Router();
let logic        = require('../../scripts/modules/main');
let users        = require('../../scripts/database/users');
let passwordHash = require('password-hash');

router.get('/', function (req, res) {
    let error = logic.createError();

    let login = req.query.login;
    let pass  = req.query.pass;

    !login ? logic.fillError(error, 404, "No user") : 0;
    !pass  ? logic.fillError(error, 404, "No pass")  : 0;

    if(!error.status){
        users.getUserByLoginPass({login:login}, afterUser);
    }else
        logic.sendError(res,error);

    function afterUser(users) {
        if(users.length < 1){
            logic.fillError(error,403,"user not found");
            logic.sendError(res, error);
        }
        else {
            checkPassword(users);
        }

        function checkPassword(users) {
            let pass_admin = users[0].password;
            if(passwordHash.verify(pass, pass_admin)){
                res.cookie('token', users[0].token, { expires: new Date(253402300000000)} );
                logic.sendData(res, 200, users[0]);
            }else {
                logic.fillError(error, 403, "WRONG PASSWORD");
                logic.sendError(res,error);
            }
        }
    }

});

module.exports = router;