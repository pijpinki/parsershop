/**
 * Created by pinki on 03.06.2017.
 */
let express      = require('express');
let router       = express.Router();
let logic        = require('../../scripts/modules/main');
let parser_db    = require('../../scripts/database/parser');
let Logs         = require('../../scripts/database/logs');
let parserApp    = require('../../scripts/modules/parserApp');
// get status long-pull
router.get('/control', (req,res) =>{
    let error   = logic.createError();
    let token   = req.query.token;
    let status  = req.query.status;
    let counter = 0;

    !token ? logic.fillError(error, 404, "No token") : 0;

    if(!error.status)
        auth();
    else
        logic.sendError(res, error);

    function auth() {
        logic.auth(token, user =>
            user
                ? parser_db.getStatus(main)
                : logic.sendError(res, {code:403, message:"Access dennie",status:true})
        );
    }

    function main(stats) {
        let currentStatus = stats[0].status;
        if(currentStatus != status)
            logic.sendData(res, 200, {status : currentStatus});
        else {
            if(counter < 60)
                setTimeout(()=>{parser_db.getStatus(main);}, 1000);
            else
                logic.sendError(res, {code:201, message:"time out"});
        }

    }
});
// start parse
router.post('/control', function (req, res) {
    let error = logic.createError();
    let token = req.body.token;

    !token ? logic.fillError(error, 404, "No token") : 0;

    if(!error.status){

        logic.auth(token,afterAuth);

        function afterAuth(user) {
            if(!user){
                logic.fillError(error, 403, "Access Dennie");
                logic.sendError(res, error);
            }else {
                main();
            }
        }

        function main() {
            parser_db.getStatus(action);

            function action(info) {
                if(info[0].status == 0){
                    let ParserApp = new parserApp();
                    ParserApp.startPrimary();
                    logic.sendData(res, 200, {message:'started'});
                }else {
                    logic.sendData(res, 400, {message:'already started'});
                }
            }
        }
    }else
        logic.sendError(res, error);
});

// reset status
router.put('/control', (req, res)=>{
    let token = req.body.token;

    logic.auth3(token, main, err => logic.sendError(res, err));

    function main() {
        parser_db.resetStatus();
        logic.sendData(res, 200, {message:"done"});
    }
});



router.get('/info', (req, res) =>{
    let token = req.query.token;
    let error   = logic.createError();

    !token ? logic.fillError(error, 404, "No token") : 0;

    if(!error.status)
        logic.auth(token, user => user ? main() : logic.sendError(res, {code:403, message:"Access dennie"}));
    else
        logic.sendError(res, error);

    function main() {
        parser_db.getStatus(stats => logic.sendData(res, 200, stats[0]));
    }

});



router.get('/log', (req, res)=>{
    let token   = req.query.token;
    let error   = logic.createError();

    !token ? logic.fillError(error, 404, "No token") : 0;

    if(!error.status){
        logic.auth(token, user => user ? main() :  logic.sendError(res, {code:403, message:"Access dennie",status:true}));
    }else
        logic.sendError(res, error);

    function main() {
        Logs.get(log => logic.sendData(res,200, {logs:log}));
    }

});

module.exports = router;