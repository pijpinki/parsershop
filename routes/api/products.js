/**
 * Created by pinki on 05.06.2017.
 */
let express      = require('express');
let router       = express.Router();

let logic        = require('../../scripts/modules/main');

let Products     = require('../../scripts/database/producs');


router.get('/new', (req, res) =>{
    let error   = logic.createError();
    let token   = req.query.token;

    !token ? logic.fillError(error, 404, "No token") : 0;

    if(!error.status)
        logic.auth(token, user => user ? main() : logic.sendError(res, {code:403, message:"Access dennie"}));
    else
        logic.sendError(res, error);

    function main() {
        Products.getNew(products => {
            logic.sendData(res, 200, {products:products});
        });
    }
});

router.get('/updated', (req, res) =>{
    let error   = logic.createError();
    let token   = req.query.token;

    !token ? logic.fillError(error, 404, "No token") : 0;

    if(!error.status)
        logic.auth(token, user => user ? main() : logic.sendError(res, {code:403, message:"Access dennie"}));
    else
        logic.sendError(res, error);

    function main() {
        Products.getUpdated(products => {
            logic.sendData(res, 200, {products:products});
        });
    }
});

module.exports = router;