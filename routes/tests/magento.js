/**
 * Created by pinki on 25.06.2017.
 */
let express = require('express');
let router  = express.Router();
let magento = require('../../scripts/modules/magentoV2');
let Magento = new magento();

/* GET home page. */
router.get('/', function(req, res, next) {
    Magento.getOptionList();
    res.render('tests/index');
});

router.delete('/', function (req, res) {
   Magento.deleteAllProducts(1);
   res.send({code:201, data : { message : "Processing"}});
});

router.post('/get/sku', function (req,res) {

    magento.getProductInfoSku(471860522, (err, data) => {
       if(err) throw err;
       res.send(data);
    });
});


router.get('/start', (req, res) => {
    let parserApp = require('../../scripts/modules/parserApp');
    let App = new parserApp();
    App.startAll(()=>console.log("<1-----------------1>"));
    res.send('started');
});

module.exports = router;