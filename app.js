let express      = require('express');
let path         = require('path');
let favicon      = require('serve-favicon');
let logger       = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser   = require('body-parser');
let passwordHash = require('password-hash');
let fs           = require('fs');


let index = require('./routes/index');
let users = require('./routes/users');
let login = require('./routes/login');

let api = {};
api.user     = require('./routes/api/user');
api.parser   = require('./routes/api/paerse');
api.products = require('./routes/api/products');

let tests = {
    magento : require('./routes/tests/magento')
};


let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/node', express.static(__dirname + '/node_modules/'));

app.use('/', index);
app.use('/users', users);
app.use('/login', login);

app.use('/api/user'     , api.user);
app.use('/api/parser'   , api.parser);
app.use('/api/products' , api.products);

app.use('/tests/magento', tests.magento);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

console.log(passwordHash.generate('123qqq'));
console.log('login    : admin');
console.log('password : 123qqq');

let ParserApp = require('./scripts/modules/parserApp');
let P = new ParserApp();
P.resume();
setTimeout(P.process.bind(P),3000);


module.exports = app;



